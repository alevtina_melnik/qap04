a=input('Enter anything: ')
try:
    b = int(a)
    if b < 0:
        print('Exact number is negative')
    else:
        if (b % 2) == 0:
            print('Exact number is even')
        else:
            print('Exact number is odd')
except ValueError:
    print("Only numbers are supported")