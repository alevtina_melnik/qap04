'''
Create a car with color, model, current mileage, max mileage. By default max mileage=current mileage + 1000
Car should have 2 methods:
1. print_info: which will print all information about car e.g “This is color model car”
2. drive. Which accepts kms to drive. Method should print any message every km it has drive.
If it is more or equals max mileage then car is broken and should print it
'''


class Car:

    def print_info(self, color, model, mileage):
        max_mileage = mileage + 1000
        print(
            f'\nInformation about the car:\n \tColor: {color}\n \tModel: {model}\n \tCurrent mileage: {mileage}\n'
            f'\tMax mileage: {max_mileage}'
        )

    def drive(self, km, mileage):
        max_mileage = mileage + 1000
        while km > 0:
            if mileage < max_mileage:
                mileage += 1
                print(f'1  more km is driven, my current mileage is {mileage}')
                km -= 1
            else:
                print(f'I’m broken because my current mileage is maximum ({max_mileage})')
                break
