#print all comparison operations result
#e.g
#print(5 > 9)
#print(2 != 3)
#e.t.c
print(1 < 3)
print(1 <= 3)
print(1 == 3)
print(1 != 3)
print(1 > 3)
print(1 >= 3)
