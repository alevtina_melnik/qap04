class Car:
    def __init__(self, color, model, current_mileage, max_mileage=None):
        self.color = color
        self.model = model
        self.current_mileage = current_mileage
        self.max_mileage = max_mileage

        if not self.max_mileage:
            self.max_mileage = self.current_mileage + 1000

    def print_info(self):
        print(
            f'This is {self.color} {self.model} current mileage-{self.current_mileage} max mileage-{self.max_mileage} car')

    def drive(self, kms):
        for i in range(kms):
            if i + 1 >= self.max_mileage:
                print('Broken')
                break
            print('Drive')


car1 = Car(color='green', model='tesla', current_mileage=15, max_mileage=2)
car2 = Car(color='red', model='bmw', current_mileage=15, max_mileage=20)

car1.print_info()
car2.print_info()

car1.drive(3)
car2.drive(2)