# 4. Даны два действительных числа. Найти среднее арифметическое и среднее геометрическое этих чисел

a = 5
b = 3

average = (a + b) / 2
geom_average = (a * b) ** (1 / 2)

print(average)
print(geom_average)
