class Human:

    @staticmethod #не использует объект класс, а просто как обычная функция
    def walk(km):
        print(f"I went {km} kms")
Human.walk(15)
Human.walk(20)